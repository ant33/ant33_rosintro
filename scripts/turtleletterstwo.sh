rosservice call /reset
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[-2,2,0.0]' '[0.0,0.0,0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[-2,-2,0.0]' '[0.0,0.0,0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[1,1,0.0]' '[0.0,0.0,0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[2,0,0.0]' '[0.0,0.0,0.0]'
rosservice call /spawn 8 2 0 ""
rosservice call turtle2/set_pen 255 0 0 1 0
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist '[0,4,0.0]' '[0.0,0.0,0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist '[-2,0,0.0]' '[0.0,0.0,0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist '[4,0,0.0]' '[0.0,0.0,0.0]'
