rosservice call /reset
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[-2,2,0.0]' '[0.0,0.0,0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[-2,-2,0.0]' '[0.0,0.0,0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[1,1,0.0]' '[0.0,0.0,0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[2,0,0.0]' '[0.0,0.0,0.0]'